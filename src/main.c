//
// Created by emnhaque on 30.12.2021.
//
#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "tester.h"

/***Khi chúng ta sử dụng """restric""" với một con trỏ ptr, nó cho trình biên dịch biết rằng ptr là cách duy nhất để truy cập đối
 * tượng được trỏ bởi nó, nói cách khác, không có con trỏ nào khác trỏ đến cùng một đối tượng, tức là từ khóa hạn chế chỉ
 * định rằng một đối số con trỏ cụ thể thực hiện. không phải bí danh nào khác và trình biên dịch không cần thêm bất kỳ kiểm tra bổ sung nào.
 * */
#define HEAP_INIT_SIZE 1024

int main() {
    init_heap_test();

    struct block_header *heap_start = NULL;
    heap_start = heap_init(HEAP_INIT_SIZE);
    if (normal_memory_allocation_successful(heap_start) == SUCCESS_TEST &&
        freeing_one_block_from_several_allocated_ones(heap_start) == SUCCESS_TEST &&
        freeing_two_block_from_several_allocated_ones(heap_start) == SUCCESS_TEST &&
        grow_heap_1(heap_start) == SUCCESS_TEST &&
        grow_heap_2(heap_start) == SUCCESS_TEST) {

        printf("ALL test is success ! Congratulation !\n");
        return SUCCESS_TEST;
    }

    return 0;
}
