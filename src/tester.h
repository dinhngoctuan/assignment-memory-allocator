//
// Created by emnhaque on 30.12.2021.
//
#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#include "mem.h"
#include "mem_internals.h"


enum result_of_test {
    SUCCESS_TEST = 0,
    UNSUCCESSFUL_TEST,
};
void print_status_of_test(enum result_of_test status);
void print_test(char* const header);
enum result_of_test  init_heap_test ();
enum result_of_test  normal_memory_allocation_successful(struct block_header *heap_start);
enum result_of_test freeing_one_block_from_several_allocated_ones(struct block_header* heap_start);
enum result_of_test  freeing_two_block_from_several_allocated_ones(struct block_header* heap_start);
enum result_of_test  grow_heap_1(struct block_header* heap_start);
enum result_of_test grow_heap_2(struct block_header* heap_start);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
