//
// Created by emnhaque on 30.12.2021.
//

#include <stdio.h>
#include "tester.h"
#include "mem_internals.h"
#include "util.h"
#include "mem.h"
#include <unistd.h>
#include <sys/mman.h>


void print_test(char* const header) {
    static size_t n = 1;
    printf("..................Test ###%zu ...................\n%s\n ", n ,header);
    n++;
}

static char* const test_status_string[] = {
        [SUCCESS_TEST] = "successful test run congrats !\n",
        [UNSUCCESSFUL_TEST] = "Test failed, please try again !"
};

void print_status_of_test(enum result_of_test status){
    printf("%s",test_status_string[status]);
}

static inline struct block_header* get_block_header(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

static void allocate_pages_next(struct block_header *blockHeader) {
    void *addr = (uint8_t *) blockHeader + size_from_capacity(blockHeader->capacity).bytes;
    addr = (uint8_t *) (sysconf(_SC_PAGESIZE) * ((size_t)addr / sysconf(_SC_PAGESIZE)+ ((size_t)addr % sysconf(_SC_PAGESIZE)) > 0));
    addr = mmap((void *)addr,512, PROT_READ | PROT_WRITE,MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
}

enum result_of_test init_heap_test (){
    print_test("Test Heap Init !");

    void *heap = heap_init(1024);
    if(heap == NULL){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    debug_heap(stdout, heap);
    print_status_of_test(SUCCESS_TEST);
    return SUCCESS_TEST;
}
enum result_of_test  normal_memory_allocation_successful(struct block_header *heap_start){
    print_test("Нормальное успешное выделение памяти.");
    void* block1 = _malloc(100);
    void *block2 = _malloc(12);
    struct block_header *header = get_block_header(block2);
    if(block1 == NULL || block2 ==NULL  || header->capacity.bytes != 24){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    if(heap_start->is_free){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    debug_heap(stdout, heap_start);
    print_status_of_test(SUCCESS_TEST);
    _free(block1);
    _free(block2);
    return SUCCESS_TEST;
}

enum result_of_test  freeing_one_block_from_several_allocated_ones(struct block_header* heap_start){
    print_test("Освобождение одного блока из нескольких выделенных.");
    void *block1 = _malloc(1024);
    void *block2 = _malloc(1024);
    if(!block1 || !block2){
        _free(block1);
        _free(block2);
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    _free(block1);
    debug_heap(stdout,heap_start);
    struct block_header *block_header_1 = get_block_header(block1);
    struct block_header *block_header_2 = get_block_header(block2);
    if(!block_header_1->is_free || block_header_2 ->is_free ){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    print_status_of_test(SUCCESS_TEST);
    _free(block2);
    return SUCCESS_TEST;
}
enum result_of_test  freeing_two_block_from_several_allocated_ones(struct block_header* heap_start){
    print_test("освобождение двух блоков из нескольких выделенных");
    void *block1 = _malloc(256);
    void *block2 = _malloc(512);
    void *block3 = _malloc(1024);
    if(!block1 || !block2 || !block3){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    _free(block1);
    _free(block2);
    debug_heap(stdout, heap_start);
    struct block_header *blockHeader_1 = get_block_header(block1);
    struct block_header *blockHeader_2 = get_block_header(block2);
    struct block_header *blockHeader_3 = get_block_header(block3);
    if(! (blockHeader_1->is_free) || !(blockHeader_2 -> is_free) || blockHeader_3 ->is_free){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    print_status_of_test(SUCCESS_TEST);
    _free(block1);
    _free(block2);
    _free(block3);
    return SUCCESS_TEST;
}

enum result_of_test grow_heap_1(struct block_header* heap_start){
    print_test("Память закончилась, новая область памяти расширяет старую.");
    _malloc(128);
    _malloc(256);
    _malloc(512);
    _malloc(1024);
    debug_heap(stdout, heap_start);
    print_status_of_test(SUCCESS_TEST);
    return SUCCESS_TEST;
}

enum result_of_test grow_heap_2(struct block_header* heap_start){
    print_test("Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, ....");
    size_t query = 2056;
    void *block1 = _malloc(query);
    if(block1 == NULL){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    struct block_header *blockHeader = heap_start;
    while(blockHeader->next != NULL){
        blockHeader = blockHeader->next;
    }
    allocate_pages_next(blockHeader);

    void *allocated = _malloc(query);
    struct block_header *block2 = get_block_header(allocated);

    if(block2 == blockHeader){
        print_status_of_test(UNSUCCESSFUL_TEST);
        return UNSUCCESSFUL_TEST;
    }
    debug_heap(stdout,blockHeader);
    _free(block1);
    _free(block2);
    print_status_of_test(SUCCESS_TEST);
    return SUCCESS_TEST;
}



