#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...); 
/** Get size from capacity**/
extern inline block_size size_from_capacity(block_capacity cap);
/**Get capacity from size of block**/
extern inline block_capacity capacity_from_size(block_size sz);

/** xem cai block cap phat da du lon chua**/
static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
  /** tra ve tru neu capacity du lon, nguoc lai false **/
}
/** Hàm getpagesize () trả về kích thước trang hiện tại**/
// tra ve so trang duoc lap ra
static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
/** tra ve so trang da tao ra duoc lam tron**/
static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

// khoi tao block voi dia chi, kich thuoc,  va dia chi cua block tiep theo
static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}
// kích thước thực tế của khu vực
//  tra ve cai lon nhat cua round_pages( query ) hoac la REGION_MIN_SIZE da duoc
//  dinh san const
static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}
/**
 * Check if the region is valid
 * **/
extern inline bool region_is_invalid(const struct region *r);

/**
 * mmap hàm được sử dụng để ánh xạ giữa không gian địa chỉ quy trình và tệp hoặc
 thiết bị addr - doi sô cung cấp địa chỉ ưu tiên cho ánh xạ length - so byte
 duoc anh xa additional-flag - kiem soat tinh chat cua mmap Cac chi so cua flag
 nếu quên đọc trong link đã lưu phần flags của mmap VD - MAP_FIXED he thong băt
 buộc sử dụng địa chỉ được chỉ định nếu không ánh xạ sẽ không  thành công
**/
// On success, the mmap() returns 0; for failure, the function returns
// MAP_FAILED.
/** mmap to allocate a memory in heap  **/
/**
 * MAP_ANONYMOUS
              The mapping is not backed by any file; its contents are
              initialized to zero

              MAP_PRIVATE
               Tạo ánh xạ sao chép-ghi-chép riêng.
               Các cập nhật đối với ánh xạ không hiển thị đối với
               các quy trình khác ánh xạ cùng một tệp và không được chuyển đến tệp bên dưới.
               Không xác định được liệu các thay đổi được thực hiện đối với tệp sau lệnh gọi
               mmap () có hiển thị trong vùng được ánh xạ hay không
 **/

/** the below function return 0 if success, MAP_FAILED if unsuccessful **/
static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
// cấp phát một vùng bộ nhớ và khởi tạo nó bằng một khối chua dia chi va 1 so
// byte nhat dinh
static struct region alloc_region(void const *addr, size_t query) {
  /*  ??? */
  size_t query_actual = region_actual_size(query);
  block_capacity capacity = {query_actual};
  block_size block_sz = size_from_capacity(capacity);
  // bat buoc phai dung dia chi duoc chi dinh addr neu khong se anh xa khong thanh cong
  void *next_addr = map_pages(addr, query_actual, MAP_FIXED);
  struct region new_region;

  if (next_addr == MAP_FAILED) {
    next_addr = map_pages(addr,query,0);
    new_region = (struct region){next_addr,query,false};
  }else{
      new_region = (struct region){next_addr,query,true};
  }
  // tao block header moi voi dia chi tiep theo la null va dung luong chua tu block_size (capacity_from_size);
  block_init(next_addr,block_sz,NULL);
  return new_region;
}

static void *block_after(struct block_header const *block);

/** khoi tao bo nho heap**/
void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region)){
      return NULL;
  }
  return region.addr;
}


#define BLOCK_MIN_CAPACITY 24
/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
  return block->is_free && block->capacity.bytes >= query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  if(!block_is_big_enough(query,block)){
      return false;
  }
  /** Create a new block that starts where the current one ends **/
  if(block_splittable(block,query)){
      struct block_header *next_block = (struct block_header*)(block->contents + query);
      block_size next_size = (block_size){block->capacity.bytes - query};
      block_init(next_block,next_size,block->next);
      block->next = next_block;
      block->capacity = (block_capacity ){query};

      return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */
static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
  return (void *)snd == block_after(fst);
}

// kiem tra xem hai khoi is free khong
static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next_block = block->next;
    if(next_block && mergeable(block, next_block)){
        block->next = next_block->next;
        block->capacity = (block_capacity ){block->capacity.bytes + next_block->capacity.bytes + offsetof(struct block_header,contents)};
        return true;
    }
    return false;
}
static bool try_merge_with_next_all(struct block_header *block){
    bool flag = false;
    while(block->next){
        if(!(try_merge_with_next(block))) break;
        flag = true;
    }
    return flag;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};
/** tìm thấy tốt hoặc diem cuoi cung */
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
  struct block_search_result result_of_find = {0};
  struct block_header *current_block = block;
  do{
      try_merge_with_next_all(current_block);
      if(current_block->is_free && block_is_big_enough(sz,current_block)){
          result_of_find.type = BSR_FOUND_GOOD_BLOCK;
          result_of_find.block = current_block;
          return result_of_find;
      }
      current_block = current_block->next;
  }while(current_block -> next);

  result_of_find.type = BSR_REACHED_END_NOT_FOUND;
  result_of_find.block = current_block;

  return result_of_find;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
/**
 * co gang phan bo bo nho tren heap ma khong co gang mo rong heap
 * @param query
 * @param block
 * @return
 */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result = find_good_or_last(block,query);
    if(result.type == BSR_FOUND_GOOD_BLOCK){
        split_if_too_big(result.block,query);
    }
    return result;
}

/* Grows the heap, allocating memory for at least query and header */
/* Phát triển heap, cấp phát bộ nhớ cho ít nhất là truy vấn và tiêu đề */
static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
  query = query + offsetof(struct block_header,contents);
  void *addr = block_after(last); // *addr = last->contents + last->capacity.bytes;

  const struct region region = alloc_region(addr,query);
  if(region_is_invalid(&region)){
      return NULL;
  }
  last->next = (struct block_header*)region.addr;
  return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
/* Triển khai logic malloc chính và trả về tiêu đề của khối được cấp phát */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
  if(query < BLOCK_MIN_CAPACITY){
      query = BLOCK_MIN_CAPACITY;
  }
  struct block_search_result block_result = try_memalloc_existing(query,heap_start);

  if(block_result.type == BSR_REACHED_END_NOT_FOUND){
      if(!grow_heap(block_result.block, query)) return NULL;
      block_result = try_memalloc_existing(query, heap_start);
  }
  if(block_result.type == BSR_CORRUPTED){
      return NULL;
  }
  block_result.block->is_free = false;
  return block_result.block;
}

void *_malloc(size_t query) {
  struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
  if (!mem){
      return;
  }
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  try_merge_with_next_all(header);
}
